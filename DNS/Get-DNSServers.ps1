# This Script will be used to change DNS servers on all servers
$wsusSession = New-PSSession -ComputerName 'computerName' -Credential $cred
$cstServers = Invoke-Command -Session $wsusSession -ScriptBlock {
    Get-WsusComputer -ComputerTargetGroups 'Group' -IncludeSubgroups
}

# Create session then sets DNS servers in Order.
$s = New-pssession -computername $cstServers.FullDomainName -Credential $cred
Invoke-Command -Session $s -ScriptBlock {
    Get-WMIObject win32_NetworkAdapterConfiguration | Select-object DNSHostName,Index,Description,DNSServerSearchOrder,DefaultIPGateway  | Where-Object {$_.DefaultIPGateway -ne $null};
} -AsJob | Wait-Job | receive-job | Export-Csv -Path 'PATH' 

#Invoke-Command -Session $s -ScriptBlock {
# Write-Output "Command successful on $env:COMPUTERNAME"
#} 
