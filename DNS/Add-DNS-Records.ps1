<#
.SYNOPSIS
  Script that adds DNS records
.DESCRIPTION
  This script runs locally on your machine and add DNS records to a given DNS server
.NOTES
  Version:        1.10
  Author:         Tim Alvarado
  Creation Date:  01/17/2020
  Purpose/Change: Script to add DNS records
#>


$DNSServer = Read-Host "Enter DNS server to use:"
$IP4Address = Read-Host "Enter IP Address:"
$RRName = Read-Host "Enter desired hostname:"

Add-DnsServerResourceRecordA -ComputerName $dnsserver -ZoneName "example.com" -name $RRName -TimeToLive 00:30:00 -IPv4Address $IP4Address -AllowUpdateAny -CreatePtr
