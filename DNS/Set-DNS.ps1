# Use this script to test if servers can resolve using a specific Name Server
# Make Sure to change the wsus targetgroup group and $ns variable

$wsusSession = New-PSSession -ComputerName 'ComputerName' -Credential $cred
$servers = Invoke-Command -Session $wsusSession -ScriptBlock {
    Get-WsusComputer -ComputerTargetGroups 'GROUP' -IncludeSubgroups
}

$s = New-PSSession -ComputerName $servers.FullDomainName -Credential $cred
invoke-command -Session $s -ScriptBlock {
    $client = 'server to lookup'
    $ns1 = 'nameserver'
    $ns2 = 'nameserver'
    $ns1_output = (nslookup $client $ns1 | select-string address | select -last 1).toString().split(":")[1].trim()
    $ns2_output = (nslookup $client $ns2 | select-string address | select -last 1).toString().split(":")[1].trim()

    if ($ns1_output -eq 'ip of $client' -And $ns2_output -eq 'ip of $client') {
        Write-Host $env:COMPUTERNAME DNS settings are being changed -ForegroundColor Green
        $adapter = Get-WMIObject win32_NetworkAdapterConfiguration | Where-Object {$_.DefaultIPGateway -ne $null};
        $dns_servers = "$ns1", "$ns2";
        $adapter.SetDNSServerSearchOrder($dns_servers)
    }
    
    else {
        Write-Host $env:COMPUTERNAME cannot resolve using the new NS1 or old NS2 server. DNS Settings are not being changed -ForegroundColor Red
    }
} -AsJob -JobName 'DNS Resolves'
wait-job 'DNS Resolves' | receive-job
Get-PSSession | Remove-PSSession
