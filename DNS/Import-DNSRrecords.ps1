<#
.SYNOPSIS
  Script that adds DNS records in bulk
.DESCRIPTION
  This script runs locally on your machine and add DNS records to a given DNS server in bulk by importing a csv
.NOTES
  Version:        1.10
  Author:         Tim Alvarado
  Creation Date:  01/27/2020
  Purpose/Change: Add DNS records in bulk
#>


$DNSServer = Read-Host "Enter DNS server to use:"
$csv = Import-CSV -Path .\dns_records.csv

# Needs Error checking
ForEach ($record in $csv) {
  Add-DnsServerResourceRecordA -ComputerName $dnsserver -ZoneName $record.Zone -name $record.Name -TimeToLive 00:30:00 -IPv4Address $record.IPv4Address -AllowUpdateAny -CreatePtr
}
