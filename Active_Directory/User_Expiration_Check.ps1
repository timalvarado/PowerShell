#Requires -Module ActiveDirectory
<#
.SYNOPSIS
  Script that returns AD users account expiration date
.DESCRIPTION
  This script runs locally on your machine and will return a users expiration date.
.NOTES
  Version:        1.0 
  Author:         Tim Alvarado
  Creation Date:  12/19/2019
  Purpose/Change: Retrieve AD user account expiration date
#>

$FirstName = Read-Host "Enter users's First Name"
$LastName = Read-Host "Enter user's Last Name" 
$CurrentDate = Get-Date -Format "MM/dd/yyyy hh:mm:ss tt"

$Users = Get-ADUser -Filter "GivenName -like '$FirstName' -and Surname -like '$LastName'"  -Properties AccountExpirationDate
foreach ($User in $Users) {
  if (!$User.AccountExpirationDate) {
    $Account = "{0} is not a freelancer account" -f $User.SamAccountName
    Write-Host $Account -ForegroundColor Green
  } else {
    $ExpiringDate = "{0} account expires on {1}" -f $User.SamAccountName,$User.AccountExpirationDate
    Write-Host $ExpiringDate -ForegroundColor Yellow
  }
}
