﻿# This scripts gets all Window Servers VMs and restarts the splunk service on them

$getServers = get-vm * | Select-Object name, Powerstate, @{N="DnsName"; E={$_.ExtensionData.Guest.Hostname}}, @{N="OS"; E={$_.Guest.OSFullName}} | Where-Object {$_.PowerState -eq 'PoweredOn' -and $_.OS -like "Microsoft Windows Server*"}
$servers = $getservers | Select-Object -ExpandProperty DnsName

# Invoke-command on all those servers to restart splunkforwarder
$s = New-PSSession -ComputerName $servers -Credential $cred 
Invoke-Command -Session $s -ScriptBlock {
    Restart-Service -Name SplunkForwarder
} -AsJob -JobName "Splunk_Restart"

# Wait for job to finished before moving on
Wait-Job -Name "Splunk_Restart"

# Remove all current pssessions so they don't linger around :)
Get-Pssession | Remove-Pssession
