Start-Process -FilePath splunkforwarder.msi -Wait -ArugmentList (`
      "AGREETOLICENSE=`"Yes`"", `
      "DEPLOYMENT_SERVER=`"deployment-server:8089`"" `
      "GENRANDOMPASSWORD=`"1`"" `
      "/quiet"
      )
      
Stop-service SplunkForwarder
Remove-Item 'C:\Program Files\SplunkUniversalForwarder\etc\instance.cfg'
Remove-Item '%TEMP%\splunk.log'
Start-service SplunkForwarder
